## Paginated Books Pages App

This App was built with [Create React App](https://github.com/facebook/create-react-app) and [react-bootstrap]. 

## Setup

1. Open "Command Line" (for Windows) or "Terminal" (for Mac) and 
  `cd` into `paginated-books-pages`.

2. Install dependencies:
- `yarn install` 

3. Start server:
- `yarn start`

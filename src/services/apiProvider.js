import axios from 'axios';


const instance = axios.create({
    baseURL: 'http://nyx.vima.ekt.gr:3000/api'
});

const postRequestBooks = async ({
  page = 1,
  itemsPerPage = 20,
  filters = []
}) => {
  const res = await instance.post('/books', {
    page,
    itemsPerPage,
    filters
  });
  
  const receivedItems = itemsPerPage * page;
  const totalItems = res.data.count;

  return { 
    items: res.data, 
    moreItemsToLoad: receivedItems >= totalItems
  };
};

export default { postRequestBooks };

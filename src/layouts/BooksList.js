import React, { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import queryString from 'query-string';
import PropTypes from 'prop-types';
import {
  Button,
  Card,
  Col,
  Container,
  Jumbotron,
  Row,
  Spinner
} from 'react-bootstrap';
import api from '../services/apiProvider';
import NoData from '../components/NoData';


const BooksList = () => {

  const history = useHistory();
  const location = useLocation();
  const urlPath = window.location.pathname;
  const initialQueryString = queryString.parse(location.search);
  const initialPageNumber = Number(initialQueryString.page) || 1;

  const [loaded, setLoading] = useState(false);
  const [data, setData] = useState();
  const [currentPage, setCurrentPage] = useState(initialPageNumber);

  const currentUrl = `${urlPath}?page=${currentPage}`;

  useEffect(() => {
    setLoading(false);

    api.postRequestBooks({
        page: currentPage,
        itemsPerPage: 20,
        filters: []
      })
      .then(res => {
        setLoading(true);

        return setData(res);

      });
  }, [currentPage]);

  useEffect(() => {
    history.push(currentUrl);
  }, [history, currentUrl]);

  return (
    <Container>
      <Jumbotron className="text-primary text-center bg-light py-5 my-5 px-4 ">
        <h1 className="h1 text-center">Books Information</h1>
      </Jumbotron>
      <Row>
        <Col xs="12" sm="6" className="d-flex justify-content-end">
          <Button 
            className="py-3 mr-3 flex-fill"
            onClick={() => setCurrentPage(currentPage - 1)}
            disabled={currentPage === 1}
          >
            Prev Page
          </Button>
          <Button
            className="py3 flex-fill"
            onClick={() => setCurrentPage(currentPage + 1)}
            disabled={data ? data.moreItemsToLoad : false}
          >
            Next Page
          </Button>
        </Col>
      </Row>
      <Row className="mt-4">
        {data && loaded ? (
          data.items.books.map(book => (
            <Col xs="12" lg="6" className="mb-4" key={book.id}>
              <Card bg="light" className="w-100 h-100 shadow">
                <Card.Body>
                  <Card.Title>{book.book_title}</Card.Title>
                  <Card.Text>
                    Book author:{' '}
                    {book.book_author.map((bookAuthor) => bookAuthor)}
                  </Card.Text>
                  <Card.Text>
                    Publication year: {book.book_publication_year}
                  </Card.Text>
                  <Card.Text>
                    Publication city: {book.book_publication_city}
                  </Card.Text>
                  <Card.Text>
                    Publication country: {book.book_publication_country}
                  </Card.Text>
                  <Card.Text>Book pages: {book.book_pages}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))
        ) : (
          <Col className="flex-column justify-content-center d-flex align-items-center "
            style={{ height: '100px' }}
          >
            <Spinner variant="primary" animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
          </Col>
        )}

        {data && loaded && data.items.books.length === 0 && <NoData />}
        
      </Row>
    </Container>
  );
};

export default BooksList;


BooksList.propTypes = {
    books: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            book_title: PropTypes.string,
            book_author: PropTypes.shape({
                bookAuthor: PropTypes.string,
            }),
            book_publication_year: PropTypes.number,
            book_publication_city: PropTypes.string,
            book_publication_country: PropTypes.string,
            book_pages: PropTypes.number,
        }),
    ),
};
  
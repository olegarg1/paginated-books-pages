import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import BooksList from './layouts/BooksList';

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={BooksList} />
      </Switch>
    </Router>
  );
};

export default App;
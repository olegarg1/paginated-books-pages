import React from 'react';
import { Col, Button } from 'react-bootstrap';

const NoData = () => {

    const refreshPage = () => {
        window.location.reload(false);
    }

    return (
        <Col className="justify-content-center align-items-center d-flex flex-column" style={{ height: '100px' }} >
            
             <div className="mb-3">No results avaliable.</div>

             <Button onClick={refreshPage}>Refresh</Button>
        </Col>
     );
    }

export default NoData;
